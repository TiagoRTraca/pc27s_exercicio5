/**
 * Exemplo de Produtor/Consumidor
 * sem sincronizacao
 * 
 * Autor: Tiago
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Exemplo5  {

    
    public static void main(String [] args){
        
        //cria novo pool de threads com duas threads
        ExecutorService application = Executors.newCachedThreadPool();
        
        //cria BufferNaoSincronizado para armazenar ints
        Buffer sharedLocation = new BufferSincronizado();
        
        System.out.println("Ação\t\tSoma do Produtor\tSoma do Consumidor");;
        System.out.println("-------\t\t-----------------\t------------\n");
        
        //Executa Producer e Consumer, fornecendo-lhes acesso ao mesmo Buffer->BufferNaoSincronizado: sharedLocation
        application.execute( new Producer( sharedLocation ));
        application.execute( new Consumer( sharedLocation ));
                    
        application.shutdown();
    }//fim main
    
}//fim classe
