/**
 * Buffer
 * 
 * Autor: Tiago
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

import java.util.Random;

public class Producer implements Runnable {

    private final static Random generator = new Random();
    private final Buffer sharedLocation;
    
    public Producer( Buffer shared ){
        sharedLocation = shared;
    }
    
    @Override
    public void run() {
        int sum=0;
        
        for (int count =1; count <=10; count++){
            
            try {
                //Dorme, atribui um valor no Buffer e soma
                Thread.sleep(generator.nextInt(3000));
                sharedLocation.set( count );
                sum +=count; //incrementa a soma de valores
                System.out.printf("\t%2d\n", sum);
            } catch ( InterruptedException e){
                e.printStackTrace();
            }
        }
                    System.out.printf("\n%s %d\n%s\n",
                    "Produtor produziu valores totalizando: ", sum, "Finalizando consumidor");            
    }

    
    
}
