/**
 * UnsynchronizedBuffer
 * 
 * Autor: Tiago
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

public class BufferNaoSincronizado implements Buffer {

    private int buffer = -1; //Compartilhado pelas threads producer e consumer
    
    public void set (int value ) throws InterruptedException{
        
        System.out.printf("Produtor escreve\t%2d", value);
        buffer = value;
    };
    
    public int get () throws InterruptedException{
        System.out.printf("Consumidor lê   \t%2d", buffer);
        return buffer;
    }
    
}
